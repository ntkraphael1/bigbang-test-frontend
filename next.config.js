/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        apiHost: "localhost",
        port: "5000"
    },
};

module.exports = nextConfig
