import { NextResponse } from "next/server";

const apiHost = process.env.apiHost;

export async function GET(signal: AbortSignal) {
  const res = await fetch(`http://localhost:5000/api/users`, {
    headers: {
      "Content-Type": "application/json",
    },
    signal
  });
  const data = await res.json();
  return NextResponse.json({ data });
}
