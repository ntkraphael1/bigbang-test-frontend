import { User } from "../types/user";

export default async function UserList() {
  const users = await getUser();
  return (
    <section className="flex gap-2 flex-col min-w-[250px] min-h-[300px] justify-center bg-[#ffd972]">
      <p className="flex justify-center text-[1.5rem] text-[#0000FF] font-bold">User List:</p>
      <ul className="flex flex-col text-[#0c880c]">
        {users.map((user, index) => (
            <li className="text-center" key={index}>{user.name}</li>
        ))}
      </ul>
    </section>
  );
};

async function getUser(): Promise<User[]> {
  const url = `http://${process.env.apiHost}:${process.env.port}/api/users`;
  const responseUsers = await fetch(url, {cache: "no-cache"});
  return responseUsers.json();
}
