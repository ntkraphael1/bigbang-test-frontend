"use client"

import { useState } from "react";

export default function Counter() {
    const [count, setCount] = useState<number>(0);
    return (
        <section className="flex flex-col w-full content-center py-12 bg-[#465eeb]">
            <div className="flex justify-center gap-8">
                <button className="w-[12rem] h-[2rem] bg-[#45a538] text-white font-bold rounded cursor-pointer" onClick={() => setCount((prev) => prev + 1)}>Increment</button>
                <button className="w-[12rem] h-[2rem] bg-[#FF0000] text-white font-bold rounded cursor-pointer" onClick={() => setCount((prev) => prev - 1)}>Decrement</button>
            </div>
            <p className="text-center mt-8 font-bold text-white">Counter: {count}</p>
        </section>    
    );
};