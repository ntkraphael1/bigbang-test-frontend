import Counter from './components/Counter'
import UserList from './components/UserList'

export default function Home() {
  return (
    <main className="h-[100vh]">
       {/* @ts-expect-error Server Component */}
      <UserList />
      <Counter />
    </main>
  )
}
